package com.bmi.bmimock.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.bmi.bmimock.BmiMockApplication;
import com.bmi.bmimock.UserTestHelper;
import com.bmi.bmimock.model.User;
import com.bmi.bmimock.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BmiMockApplication.class)
public class UserServiceTest {
  private static final String USERNAME = "bob";
  private UserService userService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  @Qualifier("passwordEncoder")
  private PasswordEncoder passwordEncoder;

  private User user = UserTestHelper.defaultUser();

  @Before
  public void setUp() {
    userService = new UserService(userRepository, passwordEncoder);
  }

  @Test
  public void shouldReturnAUser() {
    userRepository.save(UserTestHelper.defaultUser());
    User retrievedUser = userService.findUserByUsername(USERNAME);
    assertThat(retrievedUser.getUsername()).isEqualTo(user.getUsername());
  }

  @Test
  public void shouldReturnNullIfUserIsNotFound() {
    User retrievedUser = userRepository.findByUsername(USERNAME);
    assertThat(retrievedUser).isEqualTo(null);
  }

  @Test
  public void shouldSaveUser() {
    userRepository.save(user);
    User retrievedUser = userRepository.findByUsername(user.getUsername());
    assertThat(retrievedUser.getId()).isEqualTo(user.getId());
  }

  @Test
  public void shouldEncodePassword() {
    userService.saveNewUser(user);
    User retrievedUser = userRepository.findByUsername(user.getUsername());
    assertTrue(passwordEncoder.matches("password123", retrievedUser.getPassword()));
  }

  @Test
  public void shouldReturnLoggedInUser() {
    User user = UserTestHelper.defaultUser();
    userRepository.save(user);
    Authentication authentication = mock(Authentication.class);
    SecurityContext securityContext = mock(SecurityContext.class);
    when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);
    when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn(user.getUsername());
    assertThat(userService.getLoggedInUser().getUsername()).isEqualTo(user.getUsername());
  }

  @Test
  public void shouldUpdateUser() {
    User user = UserTestHelper.defaultUser();
    userRepository.save(user);
    User retrievedUser = userRepository.findByUsername(user.getUsername());
    assertThat(retrievedUser.getEmail()).isEqualTo(user.getEmail());
    user.setEmail("newEmail@gmail.com");
    userService.updateUser(user);
    User updatedUser = userRepository.findByUsername(user.getUsername());
    assertThat(updatedUser.getEmail()).isEqualTo("newEmail@gmail.com");
  }

  @After
  public void cleanUp() {
    userRepository.deleteAll();
  }

}
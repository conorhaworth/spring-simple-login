package com.bmi.bmimock.service;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.bmi.bmimock.UserTestHelper;
import com.bmi.bmimock.controller.RegistrationController;
import com.bmi.bmimock.model.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

public class RegistrationServiceTest {
  @InjectMocks
  private RegistrationService registrationService;

  @Mock
  private BindingResult bindingResult;
  @Mock
  private UserService userService;

  private User user = UserTestHelper.defaultUser();
  private ModelAndView modelAndView;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    modelAndView = new ModelAndView();
  }

  @Test
  public void shouldAddObjectsIfNoErrors() {

    when(bindingResult.hasErrors()).thenReturn(false);
    registrationService.redirect(bindingResult, modelAndView, user);
    assertThat(modelAndView.getModel().get("successMessage")).isEqualTo("User has been registered successfully");
    assertThat(modelAndView.getModel().get("user")).isExactlyInstanceOf(User.class);
  }

  @Test
  public void shouldNotAddObjectsIfErrors() {
    when(bindingResult.hasErrors()).thenReturn(true);
    registrationService.redirect(bindingResult, modelAndView, user);
    assertThat(modelAndView.getModel().get("successMessage")).isEqualTo(null);
    assertThat(modelAndView.getModel().get("user")).isEqualTo(null);
  }

  @Test
  public void shouldSetViewNameIfHasErrors() {
    when(bindingResult.hasErrors()).thenReturn(true);
    registrationService.redirect(bindingResult, modelAndView, user);
    assertThat(modelAndView.getViewName()).isEqualTo(RegistrationController.VIEW_NAME);
  }

  @Test
  public void shouldSetViewNameIfHasNoErrors() {
    when(bindingResult.hasErrors()).thenReturn(false);
    registrationService.redirect(bindingResult, modelAndView, user);
    assertThat(modelAndView.getViewName()).isEqualTo(RegistrationController.VIEW_NAME);
  }

}
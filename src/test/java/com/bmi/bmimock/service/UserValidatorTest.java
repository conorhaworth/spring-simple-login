package com.bmi.bmimock.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import com.bmi.bmimock.UserTestHelper;
import com.bmi.bmimock.model.User;
import java.time.LocalDate;
import org.apache.commons.validator.routines.EmailValidator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

public class UserValidatorTest {
  @InjectMocks
  private UserValidator userValidator;

  @Mock
  private UserService userService;
  @Mock
  private EmailValidator emailValidator = EmailValidator.getInstance();

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void returnsTrueIfClassIsSupported() {
    assertTrue(userValidator.supports(User.class));
  }

  @Test
  public void returnsFalseIfClassIsNotSupported() {
    assertFalse(userValidator.supports(String.class));
  }

  @Test
  public void rejectValueIfUsernameAlreadyExists() {
    String invalidUsername = "test";
    User invalidUser = UserTestHelper.defaultUser();
    invalidUser.setUsername(invalidUsername);
    Errors errors = new BeanPropertyBindingResult(invalidUser, "user");
    when(userService.findUserByUsername(invalidUsername)).thenReturn(new User());
    userValidator.validate(invalidUser, errors);
    assertTrue(errors.hasErrors());
  }

  @Test
  public void acceptValueIfUsernameIsUnique() {
    String username = "test";
    User validUser = UserTestHelper.defaultUser();
    validUser.setUsername(username);
    Errors errors = new BeanPropertyBindingResult(validUser, "user");
    when(userService.findUserByUsername(username)).thenReturn(null);
    userValidator.validate(validUser, errors);
    assertFalse(errors.hasErrors());
  }

  @Test
  public void acceptValueIfEmailIsValid() {
    String validEmail = "test@gmail.com";
    User invalidUser = UserTestHelper.defaultUser();
    invalidUser.setEmail(validEmail);
    Errors errors = new BeanPropertyBindingResult(invalidUser, "user");
    when(emailValidator.isValid(validEmail)).thenReturn(false);
    userValidator.validate(invalidUser, errors);
    assertFalse(errors.hasErrors());
  }

  @Test
  public void rejectValueIfEmailIsInvalid() {
    String invalidEmail = "test";
    User invalidUser = UserTestHelper.defaultUser();
    invalidUser.setEmail(invalidEmail);
    Errors errors = new BeanPropertyBindingResult(invalidUser, "user");
    when(emailValidator.isValid(invalidEmail)).thenReturn(false);
    userValidator.validate(invalidUser, errors);
    assertTrue(errors.hasErrors());
  }

  @Test
  public void rejectValueIfUnder18() {
    LocalDate under18 = LocalDate.of(2005, 1, 1);
    User invalidUser = UserTestHelper.defaultUser();
    invalidUser.setDateOfBirth(under18);
    Errors errors = new BeanPropertyBindingResult(invalidUser, "user");
    userValidator.validate(invalidUser, errors);
    assertTrue(errors.hasErrors());
  }

  @Test
  public void rejectValueIfDateOfBirthIsInTheFuture() {
    LocalDate future = LocalDate.of(2019, 1, 1);
    User invalidUser = UserTestHelper.defaultUser();
    invalidUser.setDateOfBirth(future);
    Errors errors = new BeanPropertyBindingResult(invalidUser, "user");
    userValidator.validate(invalidUser, errors);
    assertTrue(errors.hasErrors());
  }

  @Test
  public void rejectValueIfDateOfBirthIsNotPresent() {
    User invalidUser = UserTestHelper.defaultUser();
    invalidUser.setDateOfBirth(null);
    Errors errors = new BeanPropertyBindingResult(invalidUser, "user");
    userValidator.validate(invalidUser, errors);
    assertTrue(errors.hasErrors());
  }

  @Test
  public void acceptValueIfDateOfBirthIsValid() {
    LocalDate future = LocalDate.of(1997, 1, 1);
    User invalidUser = UserTestHelper.defaultUser();
    invalidUser.setDateOfBirth(future);
    Errors errors = new BeanPropertyBindingResult(invalidUser, "user");
    userValidator.validate(invalidUser, errors);
    assertFalse(errors.hasErrors());
  }

  @Test
  public void acceptValueIfPhoneNumberIsPresent() {
    User validUser = UserTestHelper.defaultUser();
    Errors errors = new BeanPropertyBindingResult(validUser, "user");
    userValidator.validate(validUser, errors);
    assertFalse(errors.hasErrors());
  }

  @Test
  public void rejectValueIfPhoneNumberIsNotPresent() {
    User invalidUser = UserTestHelper.defaultUser();
    invalidUser.setPhoneNumber(null);
    Errors errors = new BeanPropertyBindingResult(invalidUser, "user");
    userValidator.validate(invalidUser, errors);
    assertTrue(errors.hasErrors());
  }


}
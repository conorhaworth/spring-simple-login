package com.bmi.bmimock.service;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

public class BmiCalculatorServiceTest {
  private BmiCalculatorService bmiCalculatorService;

  @Before
  public void setup() {
    bmiCalculatorService = new BmiCalculatorService();
  }

  @Test
  public void shouldCalculateBmi() {
    double height = 10;
    double weight = 10;
    assertThat(bmiCalculatorService.calculateBmi(height, weight)).isEqualTo(0.1);
  }
}
package com.bmi.bmimock.service;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.when;

import com.bmi.bmimock.UserTestHelper;
import com.bmi.bmimock.model.User;
import com.bmi.bmimock.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsServiceImplTest {
  @InjectMocks
  private UserDetailsServiceImpl userDetailsService;
  @Mock
  private UserRepository userRepository;
  private static final String username = "test";

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void shouldThrowAnExceptionIfUserCannotBeFound() {
    when(userRepository.findByUsername(username)).thenReturn(null);

    assertThatThrownBy(() -> {
      userDetailsService.loadUserByUsername(username);
    }).isInstanceOf(UsernameNotFoundException.class);
  }

  @Test
  public void shouldReturnUserIfFound() {
    User user = UserTestHelper.defaultUser();
    when(userRepository.findByUsername(username)).thenReturn(user);
    assertThat(userDetailsService.loadUserByUsername(username)).isEqualTo(user);
  }
}
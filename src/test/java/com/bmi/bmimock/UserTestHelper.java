package com.bmi.bmimock;

import com.bmi.bmimock.model.BmiData;
import com.bmi.bmimock.model.User;
import com.bmi.bmimock.repository.BmiDataRepository;
import com.bmi.bmimock.repository.UserRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTestHelper {
  private UserTestHelper() {}

  public static User defaultUser() {
    User user = new User();
    user.setEmail("testemail@email.com");
    user.setUsername("bob");
    user.setPhoneNumber("0123456789");
    user.setPassword("password123");
    user.setDateOfBirth(LocalDate.of(1997, 7, 4));
    return user;
  }
}

package com.bmi.bmimock.controller;

import com.bmi.bmimock.BmiMockApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

@SpringBootTest(classes = BmiMockApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class BmiCalculatorControllerTest {
  @Autowired
  private WebApplicationContext webApplicationContext;

  private MockMvc mockMvc;

  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
  }

  @Test
  public void shouldRenderCalculatorPageAndAddNewBmiData() throws Exception {
    this.mockMvc.perform(get("/calculator"))
        .andExpect(status().isOk())
        .andExpect(view().name("calculator"))
        .andExpect(model().attribute("bmiData", allOf(hasProperty("height", is(0.0)),
            hasProperty("weight", is(0.0)),
            hasProperty("bmi", is(0.0)),
            hasProperty("dateCalculated", is(nullValue())),
            hasProperty("user", is(nullValue()))
        )));
  }
}
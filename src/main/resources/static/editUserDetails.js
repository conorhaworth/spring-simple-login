function editFields() {
  var editButton = document.getElementById("edit-button");
  var confirmButton = document.getElementById("confirm-button");
  var phoneNumberField = document.getElementById("phoneNumber");
  var emailField = document.getElementById("email");

  phoneNumberField.removeAttribute("readonly");
  emailField.removeAttribute("readonly");

  confirmButton.setAttribute("style", "display: block");
  editButton.setAttribute("style", "display: none");
}
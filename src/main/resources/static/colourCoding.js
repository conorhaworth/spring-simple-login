var healthyBmiLowerBound = 18.5;
var healthyBmiUpperBound = 24.9;
var overweightBmiLowerBound = 25;
var overweightBmiUpperBound = 29.9;
var obeseLowerBound = 30;

$(document).ready(function() {
  colourCode();
});

function colourCode() {
  $('.past-results-bmi-data').each(function(i, result) {
    colourCodeResult(result);
  });

  colourCodeResult($('.new-result-bmi'));
}

function colourCodeResult(result) {
  var bmi = $(result).attr("data-bmi");
  if (bmi >= healthyBmiLowerBound && bmi <= healthyBmiUpperBound) {
    $(result).css({"backgroundColor": "green", "color": "white"})
  }
  if (bmi >= overweightBmiLowerBound && bmi <= overweightBmiUpperBound) {
    $(result).css({"backgroundColor": "orange", "color": "white"})
  }
  if (bmi >= obeseLowerBound) {
    $(result).css({"backgroundColor": "red", "color": "white"})
  }
}
package com.bmi.bmimock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@EntityScan(basePackageClasses = {BmiMockApplication.class, Jsr310JpaConverters.class})
@SpringBootApplication
public class BmiMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(BmiMockApplication.class, args);
	}
}

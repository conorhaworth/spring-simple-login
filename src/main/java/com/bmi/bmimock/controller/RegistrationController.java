package com.bmi.bmimock.controller;

import com.bmi.bmimock.model.User;
import com.bmi.bmimock.service.RegistrationService;
import com.bmi.bmimock.service.UserValidator;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegistrationController {
  public static final String VIEW_NAME = "registration";
  private RegistrationService registrationService;
  private UserValidator userValidator;

  @Autowired
  public RegistrationController(RegistrationService registrationService,
      UserValidator userValidator) {
    this.registrationService = registrationService;
    this.userValidator = userValidator;
  }

  @RequestMapping(value = "/registration", method = RequestMethod.GET)
  public ModelAndView registration(ModelAndView modelAndView) {
    User user = new User();
    modelAndView.addObject("user", user);
    modelAndView.setViewName(VIEW_NAME);
    return modelAndView;
  }

  @RequestMapping(value = "/registration", method = RequestMethod.POST)
  public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult, ModelAndView modelAndView) {
    userValidator.validate(user, bindingResult);
    registrationService.redirect(bindingResult, modelAndView, user);
    return modelAndView;
  }
}

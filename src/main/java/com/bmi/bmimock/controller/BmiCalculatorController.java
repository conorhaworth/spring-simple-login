package com.bmi.bmimock.controller;

import com.bmi.bmimock.model.BmiData;
import com.bmi.bmimock.model.User;
import com.bmi.bmimock.repository.BmiDataRepository;
import com.bmi.bmimock.service.BmiDataService;
import com.bmi.bmimock.service.UserService;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

@Controller
public class BmiCalculatorController {

  public static final String RESULT = "result";
  private BmiDataRepository bmiDataRepository;
  private UserService userService;
  private BmiDataService bmiDataService;

  @Autowired
  public BmiCalculatorController(UserService userService, BmiDataRepository bmiDataRepository, BmiDataService bmiDataService) {
    this.userService = userService;
    this.bmiDataRepository = bmiDataRepository;
    this.bmiDataService = bmiDataService;
  }

  @RequestMapping(value = "/calculator", method = RequestMethod.GET)
  public ModelAndView calculator(ModelAndView modelAndView) {
    modelAndView.addObject("bmiData", new BmiData());
    modelAndView.setViewName("calculator");
    return modelAndView;
  }

  @RequestMapping(value = "/calculate", method = RequestMethod.POST)
  public ModelAndView calculate(BmiData bmiData, BindingResult bindingResult, ModelAndView modelAndView, RedirectAttributes redirectAttributes) {
    redirectAttributes.addFlashAttribute(RESULT, bmiDataService.createBmiData(bmiData));
    modelAndView.setViewName("redirect:results");
    return modelAndView;
  }

  @RequestMapping(value = "/results", method = RequestMethod.GET)
  public ModelAndView results(ModelAndView modelAndView, HttpServletRequest request) {
    User user = userService.getLoggedInUser();
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    if (inputFlashMap != null) {
      BmiData result = (BmiData) inputFlashMap.get(RESULT);
      modelAndView.addObject(RESULT, result);
    }

    List<BmiData> pastResults = bmiDataRepository.findAllByUser(user);
    pastResults.sort(Comparator.comparing(BmiData::getDateCalculated).reversed());

    modelAndView.addObject("resultHistory", pastResults);

    modelAndView.setViewName("results");
    return modelAndView;
  }
}

package com.bmi.bmimock.controller;

import com.bmi.bmimock.model.User;
import com.bmi.bmimock.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
  private UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @RequestMapping(value = "/details", method = RequestMethod.GET)
  public ModelAndView userDetails(ModelAndView modelAndView) {
    User user = userService.getLoggedInUser();
    modelAndView.addObject("user", user);
    modelAndView.setViewName("details");
    return modelAndView;
  }

  @RequestMapping(value = "/details", method = RequestMethod.POST)
  public ModelAndView editDetails(ModelAndView modelAndView, @ModelAttribute("user") User newDetails) {
    User user = userService.getLoggedInUser();
    user.setEmail(newDetails.getEmail());
    user.setPhoneNumber(newDetails.getPhoneNumber());
    userService.updateUser(user);
    modelAndView.setViewName("redirect:details");
    return modelAndView;
  }
}

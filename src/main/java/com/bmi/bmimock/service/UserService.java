package com.bmi.bmimock.service;

import com.bmi.bmimock.model.User;
import com.bmi.bmimock.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserService {
  private UserRepository userRepository;

  @Qualifier("passwordEncoder")
  private PasswordEncoder passwordEncoder;

  @Autowired
  public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  public User findUserByUsername(String username) {
    return userRepository.findByUsername(username);
  }

  public void saveNewUser(User user) {
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    userRepository.save(user);
  }

  public User getLoggedInUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    return findUserByUsername(auth.getName());
  }

  public void updateUser(User user) {
    userRepository.save(user);
  }
}
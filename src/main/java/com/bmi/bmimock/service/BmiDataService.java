package com.bmi.bmimock.service;

import com.bmi.bmimock.model.BmiData;
import com.bmi.bmimock.model.User;
import com.bmi.bmimock.repository.BmiDataRepository;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BmiDataService {
  private BmiCalculatorService bmiCalculatorService;
  private BmiDataRepository bmiDataRepository;
  private UserService userService;

  @Autowired
  public BmiDataService(BmiCalculatorService bmiCalculatorService,
      BmiDataRepository bmiDataRepository, UserService userService) {
    this.bmiCalculatorService = bmiCalculatorService;
    this.bmiDataRepository = bmiDataRepository;
    this.userService = userService;
  }

  public BmiData createBmiData(BmiData bmiData) {
    double bmi = bmiCalculatorService.calculateBmi(bmiData.getHeight(), bmiData.getWeight());
    User user = userService.getLoggedInUser();
    BmiData result = new BmiData(bmiData.getHeight(), bmiData.getWeight(), bmi, LocalDateTime.now(), user);
    bmiDataRepository.save(result);
    return result;
  }
}

package com.bmi.bmimock.service;

import static com.bmi.bmimock.controller.RegistrationController.VIEW_NAME;

import com.bmi.bmimock.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

@Service
public class RegistrationService {
  private UserService userService;

  @Autowired
  public RegistrationService(UserService userService) {
    this.userService = userService;
  }

  public void redirect(BindingResult bindingResult, ModelAndView modelAndView, User user) {
    if (bindingResult.hasErrors()) {
      modelAndView.setViewName(VIEW_NAME);
    } else {
      userService.saveNewUser(user);
      modelAndView.addObject("successMessage", "User has been registered successfully");
      modelAndView.addObject("user", new User());
      modelAndView.setViewName(VIEW_NAME);
    }
  }
}

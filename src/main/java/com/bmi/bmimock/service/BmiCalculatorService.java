package com.bmi.bmimock.service;

import org.springframework.stereotype.Service;

@Service
public class BmiCalculatorService {

  public double calculateBmi(double height, double weight) {
    return weight/(height*height);
  }

}

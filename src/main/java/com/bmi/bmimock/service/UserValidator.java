package com.bmi.bmimock.service;

import com.bmi.bmimock.model.User;
import java.time.LocalDate;
import java.time.Period;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Slf4j
@Service
public class UserValidator implements Validator {

  public static final String ERROR_USER = "error.user";
  public static final String DATE_OF_BIRTH = "dateOfBirth";
  private UserService userService;

  @Autowired
  public UserValidator(UserService userService) {
    this.userService = userService;
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return User.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    User user = (User) o;
    isUniqueUsername(user, errors);
    isUnder18(user, errors);
    isValidPhoneNumber(user, errors);
    isValidDateOfBirth(user, errors);
    isValidEmail(user, errors);
  }

  private void isUniqueUsername(User user, Errors errors) {
    User userExists = userService.findUserByUsername(user.getUsername());
    if (userExists != null) {
      errors.rejectValue("username", ERROR_USER,
          "There is already a user registered with that username");
    }
  }

  private void isValidEmail(User user, Errors errors) {
    EmailValidator emailValidator = EmailValidator.getInstance();
    if (!emailValidator.isValid(user.getEmail())) {
      errors.rejectValue("email", ERROR_USER,
          "You must enter a valid email");
    }
  }

  private void isValidDateOfBirth(User user, Errors errors) {
    if (user.getDateOfBirth() == null) {
      errors.rejectValue(DATE_OF_BIRTH, ERROR_USER,
          "You must enter your date of birth");
    } else if (user.getDateOfBirth().isAfter(LocalDate.now())) {
      errors.rejectValue(DATE_OF_BIRTH, ERROR_USER,
          "Your date of birth must be in the past");
    }
  }

  private void isUnder18(User user, Errors errors) {
    if (user.getDateOfBirth() != null) {
      int userAge = Period.between(user.getDateOfBirth(), LocalDate.now()).getYears();
      if (userAge < 18) {
        errors.rejectValue(DATE_OF_BIRTH, ERROR_USER,
            "You need to be over 18 to use this service");
      }
    }
  }

  private void isValidPhoneNumber(User user, Errors errors) {
    if(user.getPhoneNumber() == null) {
      errors.rejectValue("phoneNumber", ERROR_USER, "Please enter a valid phone number");
    }
  }
}

package com.bmi.bmimock.repository;

import com.bmi.bmimock.model.BmiData;
import com.bmi.bmimock.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BmiDataRepository extends JpaRepository<BmiData, Integer>{
  List<BmiData> findAllByUser(User user);
}

package com.bmi.bmimock.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "bmi_data")
public class BmiData {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private double height;
  private double weight;
  private double bmi;
  @Column(name = "date_calculated")
  private LocalDateTime dateCalculated;
  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  public BmiData(double height, double weight, double bmi, LocalDateTime dateCalculated,
      User user) {
    this.height = height;
    this.weight = weight;
    this.bmi = bmi;
    this.dateCalculated = dateCalculated;
    this.user = user;
  }
}

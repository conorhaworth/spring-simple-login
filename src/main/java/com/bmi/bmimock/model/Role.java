package com.bmi.bmimock.model;

import org.springframework.security.core.GrantedAuthority;

public class Role implements GrantedAuthority {
  private String role = "USER";

  @Override
  public String getAuthority() {
    return role;
  }
}
